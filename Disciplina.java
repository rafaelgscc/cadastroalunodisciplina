import java.util.ArrayList;
public class Disciplina{
	
	//atributos
	private ArrayList<Aluno> listaAlunoDisciplina;
	private String nome;
	private String horario;
	private int codigo;

	//construtor
	public Disciplina (){
		listaAlunoDisciplina=new Arraylist<Aluno>;
	}
	public Disciplina (String umNome, int umCodigo){
		nome = umNome;
		codigo = umCodigo;
	}
	public Disciplina (String umNome, String umHorario, int umCodigo){
		nome= umNome;
		horario = umHorario;
		codigo = umCodigo;
	}

	public void setNome(String umNome){
		nome=umNome;
	}
	
	public String getNome(){
		return nome;
	}
	
	public void setHorario(String umHorario){
		horario = umHorario;
	}

	public String getHorario(){
		return horario;
	}
	
	public void setCodigo (int umCodigo){
		codigo = umCodigo;
	}
	
	public int getCodigo(){
		return codigo;
	}

}
